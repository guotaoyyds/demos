import React, { Component } from 'react'
import RouterViews from "../../router/RouterViews"
export default class Home extends Component {
    render() {
        return (
            <div>
                <RouterViews router={this.props.router}/>
            </div>
        )
    }
}
